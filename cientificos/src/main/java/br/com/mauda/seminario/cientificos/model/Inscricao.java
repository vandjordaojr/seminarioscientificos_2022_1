package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

import java.time.LocalDateTime;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;

    private SituacaoInscricaoEnum situacaoInscricao;
    private Estudante estudante;
    private Seminario seminario;

    public Boolean getDireitoMaterial() {
        return direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return dataCheckIn;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return situacaoInscricao;
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public Seminario getSeminario() {
        return seminario;
    }

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.situacaoInscricao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCriacao = LocalDateTime.now();
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.situacaoInscricao = SituacaoInscricaoEnum.COMPRADO;
        this.dataCompra = LocalDateTime.now();
        this.estudante.adicionarInscricao(this);
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacaoInscricao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCompra = null;
    }
    public void realizarCheckIn() {
        this.situacaoInscricao = SituacaoInscricaoEnum.CHECKIN;
        this.dataCheckIn = LocalDateTime.now();
    }
}
