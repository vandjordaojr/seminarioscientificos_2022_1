package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {
    DISPONIVEL(1L, "disponivel"),
    COMPRADO(1L, "comprado"),
    CHECKIN(1L, "checkin");

    private Long id;
    private String nome;


    SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }
}
