package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {
    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return inscricoes;
    }

    public List<Professor> getProfessores() {
        return professores;
    }

    public Seminario(AreaCientifica areaCientifica, Professor professor, int qtdInscricoes) {
        adicionarAreaCientifica(areaCientifica);
        adicionarProfessor(professor);

        this.qtdInscricoes = qtdInscricoes;

        for(int i = 0; i != qtdInscricoes; i++){
            this.adicionarInscricao(new Inscricao(this));
        }
    }

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        this.areasCientificas.add(areaCientifica);
    }

    public boolean possuiAreaCientifica(AreaCientifica areaCientifica) {
        return this.areasCientificas.contains(areaCientifica);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
        professor.adicionarSeminario(this);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }
}
